﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class CallNumberCaesar
    {

        private string alpha;

        public CallNumberCaesar(string alpha)
        {
            this.alpha = alpha;
        }

        public string Encode(string code, int shift)
        {
            string temp = "";

            for (int j = 0; j < code.Length; j++)
            {
                if (code[j] == ' ')
                {
                    temp += ' ';
                }
                else
                {
                    temp += alpha[(alpha.IndexOf(code[j]) + shift) % alpha.Length];
                }
            }

            return temp;
        }

        public string Decode(string code, int shift)
        {
            string temp = "";

            for (int j = 0; j < code.Length; j++)
            {
                if (code[j] == ' ')
                {
                    temp += ' ';
                }
                else
                {
                    temp += alpha[(alpha.IndexOf(code[j]) + alpha.Length - shift) % alpha.Length];
                }
            }

            return temp;
        }

        public string DecodeAll(string code)
        {
            string variations = "";

            for (int i = 1; i < alpha.Length; i++)
            {
                variations += "Сдвиг на " + i + ": \t" + Decode(code, i) + "\n";
            }

            return variations;
        }
    }
}
