﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class CallNumberPlayfair
    {
        private string alpha = "АБВГДЕЖЗИКЛМНОПРСТУФХЦЧШЩЬЫЭЮЯ";
        char[,] keys = new char[5, 6];

        public CallNumberPlayfair(string key)
        {
            for (int i = 0, k = 0; i < 5; i++)
            {
                for (int j = 0; j < 6; j++, k++)
                {
                    if (k < key.Length)
                    {
                        alpha = alpha.Remove(alpha.IndexOf(key[k]), 1);
                        keys[i, j] = key[k];
                    }
                    else
                    {
                        keys[i, j] = alpha[k - key.Length];
                    }
                }
            }
        }

        public string Encode(string code)
        {
            string temp = "";

            do
            {
                char x = code[0];
                char y = code[1];
                code = code.Substring(2);

                int xi = 0,
                    xj = 0,
                    yi = 0,
                    yj = 0;

                for (int i = 0; i < 5; i++)
                {
                    for (int j = 0; j < 6; j++)
                    {
                        if (keys[i, j] == x)
                        {
                            xi = i;
                            xj = j;
                        }
                        else if (keys[i, j] == y)
                        {
                            yi = i;
                            yj = j;
                        }
                    }
                }

                if (xi != yi && xj != yj)
                {
                    temp += keys[xi, yj];
                    temp += keys[yi, xj];
                }
                else if (xi == yi)
                {
                    temp += keys[xi, (xj + 1) % 6];
                    temp += keys[yi, (yj + 1) % 6];
                }
                else if (xj == yj)
                {
                    temp += keys[(xi + 1) % 5, xj];
                    temp += keys[(yi + 1) % 5, yj];
                }
                else
                {
                    temp += x + y;
                }

            } while (code.Length > 1);

            temp += code;


            return temp;
        }

        public string Decode(string code)
        {
            string temp = "";

            do
            {
                char x = code[0];
                char y = code[1];
                code = code.Substring(2);

                int xi = 0,
                    xj = 0,
                    yi = 0,
                    yj = 0;

                for (int i = 0; i < 5; i++)
                {
                    for (int j = 0; j < 6; j++)
                    {
                        if (keys[i, j] == x)
                        {
                            xi = i;
                            xj = j;
                        }
                        else if (keys[i, j] == y)
                        {
                            yi = i;
                            yj = j;
                        }
                    }
                }

                if (xi != yi && xj != yj)
                {
                    temp += keys[xi, yj];
                    temp += keys[yi, xj];
                }
                else if (xi == yi)
                {
                    temp += keys[xi, (xj + 6 - 1) % 6];
                    temp += keys[yi, (yj + 6 - 1) % 6];
                }
                else if (xj == yj)
                {
                    temp += keys[(xi + 5 - 1) % 5, xj];
                    temp += keys[(yi + 5 - 1) % 5, yj];
                }
                else
                {
                    temp += x + y;
                }

            } while (code.Length > 1);

            temp += code;


            return temp;
        }
    }
}
