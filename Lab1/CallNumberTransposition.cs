﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class CallNumberTransposition
    {
        private string key;

        public CallNumberTransposition(string key)
        {
            this.key = key;
        }

        public string Encode(string code)
        {
            string temp = "";

            do
            {
                for (int i = 0; i < key.Length; i++)
                {
                    temp += code[Int32.Parse(key[i].ToString()) - 1] == ' ' ? '_' : code[Int32.Parse(key[i].ToString()) - 1];
                }

                code = code.Substring(key.Length);

            } while (code.Length >= key.Length);

            return temp;
        }

        public string Decode(string code)
        {
            string temp = "";

            do
            {
                for (int i = 1; i <= key.Length; i++)
                {
                    temp += code[key.IndexOf(i + "")] == '_' ? ' ' : code[key.IndexOf(i + "")];
                }

                code = code.Substring(key.Length);

            } while (code.Length >= key.Length);

            return temp;
        }
    }
}
