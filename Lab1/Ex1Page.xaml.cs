﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lab1
{
    /// <summary>
    /// Логика взаимодействия для Ex1Page.xaml
    /// </summary>
    public partial class Ex1Page : Page
    {
        private CallNumberCaesar ex1;

        public Ex1Page()
        {
            InitializeComponent();

            this.btnDecodeEx1.Click += BtnDecodeEx1_Click;
            this.btnEncodeEx1.Click += BtnEncodeEx1_Click;
            this.btnDecodeAllEx1.Click += BtnDecodeAllEx1_Click;

            this.txtAlphaEx1.Text = "АБВГДЕЖЗИКЛМНОПРСТУФХЦЧШЩЬЫЪЭЮЯ";
            this.txtDecodeEx1.Text = "СУВЮЪСВЩШЩАЮУСЭЭМЦБЩБВЦЪМГЯАСУЫЦЭЩР";
        }

        private void BtnDecodeEx1_Click(object sender, RoutedEventArgs e)
        {
            int shift;
            string decode = this.txtDecodeEx1.Text;
            if (Int32.TryParse(txtShiftEx1.Text, out shift))
            {
                shift %= this.txtAlphaEx1.Text.Length;
                txtShiftEx1.Text = shift.ToString();

                this.txtEncodeEx1.Text = ex1.Decode(decode, shift);
            }
            else
            {
                MessageBox.Show("Введен некорректный сдвиг", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BtnEncodeEx1_Click(object sender, RoutedEventArgs e)
        {
            int shift;
            string encode = this.txtEncodeEx1.Text;
            if (Int32.TryParse(txtShiftEx1.Text, out shift))
            {
                shift %= this.txtAlphaEx1.Text.Length;
                txtShiftEx1.Text = shift.ToString();

                this.txtDecodeEx1.Text = ex1.Encode(encode, shift);
            }
            else
            {
                MessageBox.Show("Введен некорректный сдвиг", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BtnDecodeAllEx1_Click(object sender, RoutedEventArgs e)
        {
            string decode = this.txtDecodeEx1.Text;

            MessageBox.Show(ex1.DecodeAll(decode), "Расшифровка", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void TxtAlphaEx1_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key >= Key.D0 && e.Key <= Key.D9) ;
            else if (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9) ; 
            else if (e.Key >= Key.A && e.Key <= Key.Z) ; 
            else if (e.Key == Key.OemOpenBrackets || e.Key == Key.Oem6 ||
                e.Key == Key.Oem1 || e.Key == Key.OemQuotes ||
                e.Key == Key.OemComma || e.Key == Key.OemPeriod || e.Key == Key.OemQuestion) ;
            else if (e.Key == Key.Escape || e.Key == Key.Tab || e.Key == Key.CapsLock || e.Key == Key.LeftShift || e.Key == Key.LeftCtrl ||
                e.Key == Key.LWin || e.Key == Key.LeftAlt || e.Key == Key.RightAlt || e.Key == Key.RightCtrl || e.Key == Key.RightShift ||
                e.Key == Key.Left || e.Key == Key.Up || e.Key == Key.Down || e.Key == Key.Right || e.Key == Key.Return || e.Key == Key.Delete ||
                e.Key == Key.Back || e.Key == Key.System) ; 
            else
                e.Handled = true;
        }

        private void TxtAlphaEx1_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (e.Text.ToUpper()[0] >= 'A' && e.Text.ToUpper()[0] <= 'Z' ) ;
            else if (e.Text.ToUpper()[0] >= 'А' && e.Text.ToUpper()[0] <= 'Я') ;
            else
                e.Handled = true;

            if (txtAlphaEx1.Text.IndexOf(e.Text.ToUpper()) < 0) ;
            else
                e.Handled = true;
        }

        private void TxtAlphaEx1_TextChanged(object sender, TextChangedEventArgs e)
        {
            string alpha = this.txtAlphaEx1.Text;

            ex1 = new CallNumberCaesar(alpha);
        }

        private void TxtEx1_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key >= Key.D0 && e.Key <= Key.D9) ;
            else if (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9) ;
            else if (e.Key >= Key.A && e.Key <= Key.Z) ;
            else if (e.Key == Key.OemOpenBrackets || e.Key == Key.Oem6 ||
                e.Key == Key.Oem1 || e.Key == Key.OemQuotes ||
                e.Key == Key.OemComma || e.Key == Key.OemPeriod || e.Key == Key.OemQuestion) ;
            else if (e.Key == Key.Escape || e.Key == Key.Tab || e.Key == Key.CapsLock || e.Key == Key.LeftShift || e.Key == Key.LeftCtrl ||
                e.Key == Key.LWin || e.Key == Key.LeftAlt || e.Key == Key.RightAlt || e.Key == Key.RightCtrl || e.Key == Key.RightShift ||
                e.Key == Key.Left || e.Key == Key.Up || e.Key == Key.Down || e.Key == Key.Right || e.Key == Key.Return || e.Key == Key.Delete ||
                e.Key == Key.Back || e.Key == Key.System) ;
            else
                e.Handled = true;
        }

        private void TxtEx1_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = txtAlphaEx1.Text.IndexOf(e.Text.ToUpper()) < 0;
        }

        private void TxtShiftEx1_PreviewKeyDown(object sender, KeyEventArgs e)
        {

            if (e.Key >= Key.D0 && e.Key <= Key.D9) ;
            else if (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9) ;
            else if (e.Key == Key.Escape || e.Key == Key.Tab || e.Key == Key.CapsLock || e.Key == Key.LeftShift || e.Key == Key.LeftCtrl ||
                e.Key == Key.LWin || e.Key == Key.LeftAlt || e.Key == Key.RightAlt || e.Key == Key.RightCtrl || e.Key == Key.RightShift ||
                e.Key == Key.Left || e.Key == Key.Up || e.Key == Key.Down || e.Key == Key.Right || e.Key == Key.Return || e.Key == Key.Delete ||
                e.Key == Key.Back || e.Key == Key.System) ; 
            else
                e.Handled = true;
        }

    }
}
