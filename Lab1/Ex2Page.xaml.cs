﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lab1
{
    /// <summary>
    /// Логика взаимодействия для Ex2Page.xaml
    /// </summary>
    public partial class Ex2Page : Page
    {
        private CallNumberPlayfair ex2;

        public Ex2Page()
        {
            InitializeComponent();

            this.btnDecodeEx2.Click += BtnDecodeEx2_Click; ;
            this.btnEncodeEx2.Click += BtnEncodeEx2_Click; ;

            this.txtKeyEx2.Text = "РЕСПУБЛИКА";
            this.txtDecodeEx2.Text = "БМИКФЛАВРСМДАМУЗЗБФЛ";
        }

        private void BtnDecodeEx2_Click(object sender, RoutedEventArgs e)
        {
            string decode = this.txtDecodeEx2.Text;

            this.txtEncodeEx2.Text = ex2.Decode(decode);
        }

        private void BtnEncodeEx2_Click(object sender, RoutedEventArgs e)
        {
            string encode = this.txtEncodeEx2.Text;

            this.txtDecodeEx2.Text = ex2.Encode(encode);
        }

        private void TxtKeyEx2_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key >= Key.A && e.Key <= Key.Z) ;
            else if (e.Key == Key.OemOpenBrackets || e.Key == Key.Oem6 ||
                e.Key == Key.Oem1 || e.Key == Key.OemQuotes ||
                e.Key == Key.OemComma || e.Key == Key.OemPeriod || e.Key == Key.OemQuestion) ;
            else if (e.Key == Key.Escape || e.Key == Key.Tab || e.Key == Key.CapsLock || e.Key == Key.LeftShift || e.Key == Key.LeftCtrl ||
                e.Key == Key.LWin || e.Key == Key.LeftAlt || e.Key == Key.RightAlt || e.Key == Key.RightCtrl || e.Key == Key.RightShift ||
                e.Key == Key.Left || e.Key == Key.Up || e.Key == Key.Down || e.Key == Key.Right || e.Key == Key.Return || e.Key == Key.Delete ||
                e.Key == Key.Back || e.Key == Key.System) ;
            else
                e.Handled = true;
        }

        private void TxtKeyEx2_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (e.Text.ToUpper()[0] >= 'А' && e.Text.ToUpper()[0] <= 'Я') ;
            else
                e.Handled = true;

            if (txtKeyEx2.Text.IndexOf(e.Text.ToUpper()) < 0) ;
            else
                e.Handled = true;
        }

        private void TxtKeyEx2_TextChanged(object sender, TextChangedEventArgs e)
        {
            string key = this.txtKeyEx2.Text;

            ex2 = new CallNumberPlayfair(key);
        }

        private void TxtEx2_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key >= Key.A && e.Key <= Key.Z) ;
            else if (e.Key == Key.OemOpenBrackets || e.Key == Key.Oem6 ||
                e.Key == Key.Oem1 || e.Key == Key.OemQuotes ||
                e.Key == Key.OemComma || e.Key == Key.OemPeriod || e.Key == Key.OemQuestion) ;
            else if (e.Key == Key.Escape || e.Key == Key.Tab || e.Key == Key.CapsLock || e.Key == Key.LeftShift || e.Key == Key.LeftCtrl ||
                e.Key == Key.LWin || e.Key == Key.LeftAlt || e.Key == Key.RightAlt || e.Key == Key.RightCtrl || e.Key == Key.RightShift ||
                e.Key == Key.Left || e.Key == Key.Up || e.Key == Key.Down || e.Key == Key.Right || e.Key == Key.Return || e.Key == Key.Delete ||
                e.Key == Key.Back || e.Key == Key.System) ;
            else
                e.Handled = true;
        }

        private void TxtEx2_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "АБВГДЕЖЗИКЛМНОПРСТУФХЦЧШЩЬЫЭЮЯ".IndexOf(e.Text.ToUpper()) < 0;
        }
    }
}
