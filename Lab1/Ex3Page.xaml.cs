﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lab1
{
    /// <summary>
    /// Логика взаимодействия для Ex3Page.xaml
    /// </summary>
    public partial class Ex3Page : Page
    {
        private CallNumberTransposition ex3;

        public Ex3Page()
        {
            InitializeComponent();

            this.btnDecodeEx3.Click += BtnDecodeEx3_Click;
            this.btnEncodeEx3.Click += BtnEncodeEx3_Click;

            this.txtKeyEx3.Text = "365142";
            this.txtDecodeEx3.Text = "кссиусопэт_виово_хоедзжря__н_и";
        }

        private void BtnDecodeEx3_Click(object sender, RoutedEventArgs e)
        {
            if (!Validate())
            {
                MessageBox.Show("Введен некорректный ключ", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            string decode = this.txtDecodeEx3.Text;

            this.txtEncodeEx3.Text = ex3.Decode(decode);
        }

        private void BtnEncodeEx3_Click(object sender, RoutedEventArgs e)
        {
            if (!Validate())
            {
                MessageBox.Show("Введен некорректный ключ", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            string encode = this.txtEncodeEx3.Text.TrimEnd(new char[] { ' ' });
            this.txtEncodeEx3.Text = encode;

            while (encode.Length % this.txtKeyEx3.Text.Length != 0)
            {
                encode += ' ';
            }

            this.txtDecodeEx3.Text = ex3.Encode(encode);
        }

        private bool Validate()
        {
            for (int i = 1; i <= this.txtKeyEx3.Text.Length; i++)
            {
                if (this.txtKeyEx3.Text.IndexOf(i + "") < 0)
                    return false;
            }

            return true;
        }

        private void TxtKeyEx3_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key >= Key.D0 && e.Key <= Key.D9) ;
            else if (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9) ;
            else if (e.Key == Key.Escape || e.Key == Key.Tab || e.Key == Key.CapsLock || e.Key == Key.LeftShift || e.Key == Key.LeftCtrl ||
                e.Key == Key.LWin || e.Key == Key.LeftAlt || e.Key == Key.RightAlt || e.Key == Key.RightCtrl || e.Key == Key.RightShift ||
                e.Key == Key.Left || e.Key == Key.Up || e.Key == Key.Down || e.Key == Key.Right || e.Key == Key.Return || e.Key == Key.Delete ||
                e.Key == Key.Back || e.Key == Key.System) ; 
            else
                e.Handled = true;
        }

        private void TxtKeyEx3_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text) || txtKeyEx3.Text.IndexOf(e.Text) != -1;
        }

        private void TxtKeyEx3_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (Validate())
            {
                this.txtKeyEx3.Background = new SolidColorBrush(Color.FromArgb(100, 0, 255, 0));
            }
            else
            {
                this.txtKeyEx3.Background = new SolidColorBrush(Color.FromArgb(100, 255, 0, 0));
            }

            string key = this.txtKeyEx3.Text;

            ex3 = new CallNumberTransposition(key);
        }

        private void TxtEx3_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key >= Key.A && e.Key <= Key.Z) ;
            else if (e.Key == Key.OemOpenBrackets || e.Key == Key.Oem6 ||
                e.Key == Key.Oem1 || e.Key == Key.OemQuotes ||
                e.Key == Key.OemComma || e.Key == Key.OemPeriod || e.Key == Key.OemQuestion) ;
            else if (e.Key == Key.Escape || e.Key == Key.Tab || e.Key == Key.CapsLock || e.Key == Key.LeftShift || e.Key == Key.LeftCtrl ||
                e.Key == Key.LWin || e.Key == Key.LeftAlt || e.Key == Key.RightAlt || e.Key == Key.RightCtrl || e.Key == Key.RightShift ||
                e.Key == Key.Left || e.Key == Key.Up || e.Key == Key.Down || e.Key == Key.Right || e.Key == Key.Return || e.Key == Key.Delete ||
                e.Key == Key.Back || e.Key == Key.System) ;
            else
                e.Handled = true;
        }

        private void TxtEx3_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "абвгдежзиклмнопрстуфхцчшщьыъэюя_".IndexOf(e.Text.ToLower()) < 0;
        }
    }
}
